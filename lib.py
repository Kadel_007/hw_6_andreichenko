def GiveAnswer():
    """For task 1"""
    print("Do you want to continue ? (Y/N):")
    answer = input().lower()
    if answer == "y":
        return True
    elif answer == "n":
        return False
    else:
        raise ValueError



