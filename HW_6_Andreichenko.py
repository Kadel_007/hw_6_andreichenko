# Task 1
# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
# и возвращает True/False в зависимости от того, что он ввел.
# В основном файле (пусть будет main_file.py) попросить пользователя ввести с клавиатуры строку и вывести ее на экран.
# Используя импортированную из lib.py функцию спросить у пользователя, хочет ли он повторить операцию (Y/N).
# Повторять пока пользователь отвечает Y и прекратить когда пользователь скажет N.

# Task 2
# Модифицируем ДЗ2. Напишите с помощью функций!. Помните о Single Respinsibility!
# Попросить ввести свой возраст (можно использовать константу или input()).
# Пользователь ввел значение возраста [year number] а на место [year string] нужно поставить правильный падеж существительного "год",
# который зависит от значения [year number].
# - если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести “не понимаю”
# - если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# - если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# - если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# - в любом другом случае - вывести “Оденьте маску, вам же [year number] [year string]!”


import lib

def checkInput(text: str) -> int:
    """For task 2"""
    try:
        inttext = int(text)
        return inttext
    except:
        return None

def inputYear():
    """For task 2"""
    dict_years = {
        1: "год",
        2: "года",
        3: "года",
        4: "года",
        21: "год",
        22: "года",
        23:"года",
        24:"года",
        31: "год",
        32: "года",
        33: "года",
        34: "года",
        41: "год",
        42: "года",
        43: "года",
        44: "года",
        51: "год",
        52: "года",
        53: "года",
        54: "года",
        61: "год",
        62: "года",
        63: "года",
        64: "года",
        71: "год",
        72: "года",
        73: "года",
        74: "года",
        81: "год",
        82: "года",
        83: "года",
        84: "года",
        91: "год",
        92: "года",
        93: "года",
        94: "года"
    }
    year_string = "лет"

    print("Введите ваш возраст (целое число):")
    text = input()
    inttext = checkInput(text)

    if inttext is None or inttext == 0:
        print("Не понимаю!")

    elif 0 < inttext < 7:
        try:
            if dict_years[inttext]:
                print(f"Тебе {inttext} {dict_years[inttext]}, где твои мама и папа?")
        except:
            print(f"Тебе {inttext} {year_string}, где твои мама и папа?")

    elif 7 <= inttext < 18:
        print(f"Тебе {inttext} {year_string}, а мы не продаем сигареты несовершеннолетним!")

    elif inttext > 65:
        try:
            if dict_years[inttext]:
                print(f"Вам уже {inttext} {dict_years[inttext]}, вы в зоне риска!")
        except:
            print(f"Вам уже {inttext} {year_string}, вы в зоне риска!")

    else:
        try:
            if dict_years[inttext]:
                print(f"Оденьте маску, вам же {inttext} {dict_years[inttext]}!")
        except:
            print(f"Оденьте маску, вам же {inttext} {year_string}!")





def main():
    # answer = True
    # while answer:
    #     print("Input string:")
    #     user_text = input()
    #     answer = lib.GiveAnswer()
    inputYear()

main()